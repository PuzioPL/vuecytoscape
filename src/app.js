import Vue from 'vue'
import axios from 'axios'
import router from '@/router/index'
import store from '@/store'
import { sync } from 'vuex-router-sync'
import App from '@/components/app-root'
import { FontAwesomeIcon } from '@/icons'
import UndoRedo from 'cytoscape-undo-redo'
import _ from 'underscore'
import VModal from 'vue-js-modal'

// Registration of global components
Vue.component('icon', FontAwesomeIcon)

Vue.prototype.$http = axios
Vue.prototype._ = _
Vue.use(VModal)

sync(store, router)

const app = new Vue({
    store,
    router,
    UndoRedo,
    ...App,
    VModal
})

export {
    app,
    router,
    UndoRedo,
    store,
    VModal
}
