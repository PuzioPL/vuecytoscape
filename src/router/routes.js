//import CounterExample from 'components/counter-example'
import FetchData from '@/components/fetch-data'
import HomePage from '@/components/home-page'
import About from '@/components/about'
import Graph from '@/components/graph'

export const routes = [
    { name: 'home', path: '/vuecytoscape/', component: HomePage, display: 'Home', icon: 'home', visible: true },
    //{ name: 'counter', path: '/counter', component: CounterExample, display: 'Counter', icon: 'graduation-cap' },
    { name: 'fetch-data', path: '/vuecytoscape/fetch-data', component: FetchData, display: 'Help', icon: 'question', visible: true },
    { name: 'graph', path: '/vuecytoscape/graph', component: Graph, display: 'Graph', icon: 'pen', visible: true },
    { name: 'about', path: '/vuecytoscape/about', component: About, display: 'About project', icon: 'info', visible: false }
]
