import Vue from 'vue'
//import App from './App.vue'
import { app } from './app'
//import router from './router'

Vue.config.productionTip = false

app.$mount('#app')


/*
import Vue from 'vue'
import App from './App.vue'
import router from './router'


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
*/