## CozyIndependencies

Link: https://puziopl.gitlab.io/vuecytoscape

Project for my first degree - the theme was "Graph Independencies".

"CozyIndependencies" is an app, where anyone can freely create and customize their own graph. After creating the graph, user can choose to select one of five different graph values to calculate:
- Minimal vertex cover
- Minimal edge cover
- Independent node sets
- Independent edge sets
- Minimal dominating sets

Each calculation returns resulting sets of edges or nodes, which can be clicked on, to select them directly on the graph.

The app is created entirely on a client side, using [Vue.js](https://vuejs.org/). Interactive graph editor is created using [Cytoscape.js](https://js.cytoscape.org/) library - an open source graph theory library, used for graph analysis and visualisation. 

Few of additional features:
- option to change elements colour/label
- option to automaticaly assign label to elements - either numeric (1, 2, 3, etc.) or alphabetic (a, b, c, etc.)
- option to center whole graph to the editor window
- option to undo and redo operations, either by using special buttons or by using known shortcut keys (CTRL+Z and CTRL+Y)
- option to export graph as JSON, photo or txt matrix
- option to import graph from JSON or txt matrix
